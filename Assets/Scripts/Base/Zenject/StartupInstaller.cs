﻿using Base.View;
using Leopotam.Ecs;
using Zenject;

namespace Base.Zenject
{
    public class StartupInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInstance(new EcsWorld());

            Container.Bind<RootView>()
                .FromComponentInHierarchy()
                .AsSingle();
        }
    }
}