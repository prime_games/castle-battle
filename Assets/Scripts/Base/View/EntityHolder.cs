﻿using Leopotam.Ecs;
using UnityEngine;

namespace Base.View
{
    public class EntityHolder : MonoBehaviour
    {
        public EcsEntity Entity;
    }
}