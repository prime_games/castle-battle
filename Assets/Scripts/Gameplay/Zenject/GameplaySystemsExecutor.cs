﻿using System;
using Gameplay.Army.Systems;
using Gameplay.Characters.Systems;
using Gameplay.Spawning.Systems;
using Leopotam.Ecs;
using Zenject;

namespace Gameplay.Zenject
{
    public class GameplaySystemsExecutor : ITickable, IDisposable
    {
        private readonly EcsSystems _systems;

        public GameplaySystemsExecutor(EcsWorld world,
            ObjectCreatorSystem objectCreatorSystem,
            PlayerArmyPlacerSystem playerArmyPlacerSystem,
            MovingSystem movingSystem,
            GetTargetSystem getTargetSystem,
            DeathSystem deathSystem,
            AttackSystem attackSystem,
            EnemyArmyPlacerSystem enemyArmyPlacerSystem)
        {
            _systems = new EcsSystems(world, "Gameplay Systems");

            _systems
                .Add(objectCreatorSystem)
                
                .Add(movingSystem)
                .Add(getTargetSystem)
                
                .Add(attackSystem)
                .Add(deathSystem)
                
                .Add(playerArmyPlacerSystem)
                .Add(enemyArmyPlacerSystem);

            _systems.Init();
        }

        public void Dispose()
        {
            _systems.Destroy();
        }

        public void Tick()
        {
            _systems.Run();
        }
    }
}