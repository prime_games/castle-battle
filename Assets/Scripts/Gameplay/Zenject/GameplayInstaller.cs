﻿using Gameplay.Army.Systems;
using Gameplay.Characters.Systems;
using Gameplay.Spawning.Systems;
using Zenject;

namespace Gameplay.Zenject
{
    public class GameplayInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<ObjectCreatorSystem>().AsSingle();
            Container.Bind<PlayerArmyPlacerSystem>().AsSingle();
            Container.Bind<EnemyArmyPlacerSystem>().AsSingle();

            Container.Bind<MovingSystem>().AsSingle();
            Container.Bind<GetTargetSystem>().AsSingle();
            Container.Bind<AttackSystem>().AsSingle();
            Container.Bind<DeathSystem>().AsSingle();
            
            Container.BindInterfacesAndSelfTo<GameplaySystemsExecutor>()
                .AsSingle()
                .NonLazy();
        }
    }
}
