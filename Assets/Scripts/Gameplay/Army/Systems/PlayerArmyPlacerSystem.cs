﻿using Gameplay.Army.Components;
using Gameplay.Characters.Components;
using Gameplay.Spawning.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Army.Systems
{
    public class PlayerArmyPlacerSystem : IEcsRunSystem
    {
        private int _maxInLine = 4;
        private int _maxInRow = 4;

        private float _placingOffset = 0.5f;

        private Vector3 _startPosition = new Vector3(-3, 1, 3);
        private Vector3 _spherePosition = new Vector3(0, 0.5f, 0);
        private float _sphereRadius = 1.0f;

        private int _playerArmyAmount = 0;
        private int _maxArmyAmount = 5;


        private int _currentIndex = 0;

        private EcsFilter<ObjectComponent, PlayerTag> _objectComponentPlayerTagFilter;
        private EcsFilter<ObjectComponent, TargetTag> _objectComponentTargetTag;

        public void Run()
        {
            foreach (var index in _objectComponentPlayerTagFilter)
            {
                ref var objectComponentPlayerTag = ref _objectComponentPlayerTagFilter.Get1(index);
                if (!objectComponentPlayerTag.InsideArmy)
                {
                    objectComponentPlayerTag.GameObject.transform.position = GetPositionInsideSphere();
                    objectComponentPlayerTag.InsideArmy = true;
                    _playerArmyAmount++;
                }

                if (_playerArmyAmount >= _maxArmyAmount)
                {
                    LaunchArmy();
                    _playerArmyAmount = 0;
                }
            }
        }

        private void LaunchArmy()
        {
            AddTargetingTagToArmy();
            Debug.Log("Army launched");
        }

        private void AddTargetingTagToArmy()
        {
            foreach (var index in _objectComponentPlayerTagFilter)
            {
                ref var entity = ref _objectComponentPlayerTagFilter.GetEntity(index);
                ref var targetingTag = ref entity.Get<TargetTag>();
            }
        }

        private Vector3 GetPositionInsideArmy()
        {
            var newPosition = new Vector3((_currentIndex % _maxInLine) * _placingOffset, 0,
                ((_currentIndex / _maxInLine % _maxInRow) * -_placingOffset));
            var result = newPosition + _startPosition;

            _currentIndex++;
            return result;
        }

        private Vector3 GetPositionInsideSphere()
        {
            return new Vector3(Random.Range(-_sphereRadius, _sphereRadius), 0,
                Random.Range(-_sphereRadius, _sphereRadius)) + _spherePosition;
        }
    }
}