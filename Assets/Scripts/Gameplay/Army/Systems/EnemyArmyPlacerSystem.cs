﻿using Gameplay.Army.Components;
using Gameplay.Spawning.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Army.Systems
{
    public class EnemyArmyPlacerSystem : IEcsRunSystem
    {
        private int _maxInLine = 4;
        private int _maxInRow = 4;

        private float _placingOffset = 2.0f;

        private Vector3 _startPosition = new Vector3(-3, 1, 17);

        private Vector3 _spherePosition = new Vector3(0, 0.5f, 20);
        private float _sphereRadius = 1.0f;

        private int _currentIndex = 0;

        private EcsFilter<ObjectComponent, EnemyTag> _objectComponentEnemyTagFilter;

        public void Run()
        {
            foreach (var index in _objectComponentEnemyTagFilter)
            {
                ref var objectComponentEnemyTag = ref _objectComponentEnemyTagFilter.Get1(index);
                if (!objectComponentEnemyTag.InsideArmy)
                {
                    objectComponentEnemyTag.GameObject.transform.position = GetPositionInsideSphere();
                    objectComponentEnemyTag.InsideArmy = true;
                }
            }
        }

        private Vector3 GetPositionInsideArmy()
        {
            var newPosition = new Vector3((_currentIndex % _maxInLine) * _placingOffset, 0,
                ((_currentIndex / _maxInLine % _maxInRow) * _placingOffset));
            var result = newPosition + _startPosition;

            // Debug.Log($"New position is {newPosition}\nResult is {result}");
            _currentIndex++;
            return result;
        }

        private Vector3 GetPositionInsideSphere()
        {
            return new Vector3(Random.Range(-_sphereRadius, _sphereRadius), 0,
                Random.Range(-_sphereRadius, _sphereRadius)) + _spherePosition;
        }
    }
}