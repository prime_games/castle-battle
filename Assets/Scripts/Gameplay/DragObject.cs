﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;


public class DragObject : MonoBehaviour
{
    private Vector3 mOffset;
    private float mZCoord;

    private bool IsDragging = false;

    public float minimalYValue = 0.5f;

    public UnityEvent OnMouseUpEvent;


    void OnMouseDown()
    {
        mZCoord = Camera.main.WorldToScreenPoint(
            gameObject.transform.position).z;
        IsDragging = true;


        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
    }

    void OnMouseUp()
    {
        OnMouseUpEvent.Invoke();
        IsDragging = false;
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;

        mousePoint.z = mZCoord;

        var worldPosition = Camera.main.ScreenToWorldPoint(mousePoint);
        worldPosition.y = Mathf.Max(worldPosition.y, minimalYValue);
        return worldPosition;
    }


    void OnMouseDrag()
    {
        transform.position = GetMouseAsWorldPoint() + mOffset;
    }
}