﻿using UnityEngine;

namespace Gameplay.Characters.Configs
{
    [CreateAssetMenu(fileName = "CharacteristicConfig", menuName = "Gameplay/Characters/Configs/CharacteristicConfig", order = 0)]
    public class CharacteristicConfig : ScriptableObject
    {
        [SerializeField] private float health;
        [SerializeField] private float damage;
        [SerializeField] private float attackRange;

        public float Health => health;
        public float Damage => damage;
        public float AttackRange => attackRange;
    }
}