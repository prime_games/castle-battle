﻿using Gameplay.Characters.Components;
using Gameplay.Spawning.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Characters.Systems
{
    public class AttackSystem : IEcsRunSystem
    {
        private EcsFilter<ObjectComponent, AttackComponent> _objectComponentAttackComponentFilter;

        public void Run()
        {
            foreach (var index in _objectComponentAttackComponentFilter)
            {
                ref var objectComponent = ref _objectComponentAttackComponentFilter.Get1(index);
                ref var attackComponent = ref _objectComponentAttackComponentFilter.Get2(index);
                ref var entity = ref _objectComponentAttackComponentFilter.GetEntity(index);

                ref var characteristicComponent = ref entity.Get<CharacteristicComponent>();

                if (attackComponent.TargetGameObject == null)
                {
                    entity.Del<AttackComponent>();
                    GetNewTarget(ref entity);
                }
                else
                {
                    var distanceUntilTarget = Vector3.Distance(objectComponent.GameObject.transform.position,
                        attackComponent.TargetGameObject.transform.position);

                    if (distanceUntilTarget < characteristicComponent.AttackRange)
                    {
                        MakeAttack(ref entity, ref attackComponent.TargetEntity);
                    }
                    else
                    {
                        var targetObject = attackComponent.TargetGameObject.transform;

                        entity.Del<AttackComponent>();

                        AddMovingComponent(ref entity, targetObject, characteristicComponent.AttackRange);
                        Debug.Log("Not enough attack range,  moving component added");
                    }
                }
            }
        }

        private void MakeAttack(ref EcsEntity attackerEntity, ref EcsEntity attackedEntity)
        {
            ref var attackerCharacteristicComponent = ref attackerEntity.Get<CharacteristicComponent>();
            ref var attackedCharacteristicComponent = ref attackedEntity.Get<CharacteristicComponent>();

            attackedCharacteristicComponent.Health -= attackerCharacteristicComponent.Damage;
            Debug.Log("Entity attacked another entity");
        }

        private void GetNewTarget(ref EcsEntity entity)
        {
            Debug.Log("Target is dead");
            entity.Del<MovingComponent>();
            ref var targetTag = ref entity.Get<TargetTag>();
        }

        private void AddMovingComponent(ref EcsEntity entity, Transform targetTransform, float attackRange = 2.0f)
        {
            ref var movingComponent = ref entity.Get<MovingComponent>();
            movingComponent.Target = targetTransform;
            movingComponent.AttackRange = attackRange;
        }
    }
}