﻿using Gameplay.Characters.Components;
using Gameplay.Spawning.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Characters.Systems
{
    public class DeathSystem : IEcsRunSystem
    {
        private EcsFilter<ObjectComponent, CharacteristicComponent> _objectComponentCharacteristicComponentFilter;        
        
        public void Run()
        {

            foreach (var index in _objectComponentCharacteristicComponentFilter)
            {
                ref var objectComponent = ref _objectComponentCharacteristicComponentFilter.Get1(index);
                ref var characteristicComponent = ref _objectComponentCharacteristicComponentFilter.Get2(index);

                if (characteristicComponent.Health <= 0)
                {
                    ref var entity = ref _objectComponentCharacteristicComponentFilter.GetEntity(index);
                    DestroyEntity(ref entity, objectComponent.GameObject);
                }
            }
        }

        private void DestroyEntity(ref EcsEntity entity, GameObject entityObject) 
        {
            Object.Destroy(entityObject);
            entity.Destroy();
        }
    }
}