﻿using Base.View;
using Gameplay.Characters.Components;
using Gameplay.Spawning.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Characters.Systems
{
    public class MovingSystem : IEcsRunSystem
    {
        private EcsFilter<ObjectComponent, MovingComponent> _objectMovingComponentFilter;
        private EcsFilter<ObjectComponent, AttackComponent> _objectComponentAttackComponentFilter;

        private float _distance = 2.0f;
        private float _someValue = 5.0f;

        public void Run()
        {
            foreach (var index in _objectMovingComponentFilter)
            {
                ref var objectComponent = ref _objectMovingComponentFilter.Get1(index);
                ref var movingComponent = ref _objectMovingComponentFilter.Get2(index);

                if (movingComponent.Target == null)
                {
                    ref var entity = ref _objectMovingComponentFilter.GetEntity(index);
                    GetNewTarget(ref entity);
                }
                else
                {
                    if (Vector3.Distance(objectComponent.GameObject.transform.position,
                        movingComponent.Target.transform.position) > movingComponent.AttackRange)
                    {
                        objectComponent.GameObject.transform.position = Vector3.MoveTowards(
                            objectComponent.GameObject.transform.position, movingComponent.Target.transform.position,
                            _someValue * Time.deltaTime);
                    }
                    else
                    {
                        ref var entity = ref _objectMovingComponentFilter.GetEntity(index);
                        OnTargetAchieved(ref entity);
                    }
                }
            }
        }

        private void GetNewTarget(ref EcsEntity entity)
        {
            Debug.Log("Target is dead");
            entity.Del<MovingComponent>();
            ref var targetTag = ref entity.Get<TargetTag>();
        }

        private void OnTargetAchieved(ref EcsEntity entity)
        {
            Debug.Log("Entity achieved target");
            ref var movingComponent = ref entity.Get<MovingComponent>();
            var targetObject = movingComponent.Target.gameObject;
            entity.Del<MovingComponent>();

            AddAttackComponent(ref entity, targetObject);
        }

        private void AddAttackComponent(ref EcsEntity entity, GameObject targetGameObject)
        {
            var targetEntity = targetGameObject.GetComponent<EntityHolder>().Entity;
            ref var attackComponent = ref entity.Get<AttackComponent>();
            attackComponent.TargetEntity = targetEntity;
            attackComponent.TargetGameObject = targetGameObject;
        }
    }
}