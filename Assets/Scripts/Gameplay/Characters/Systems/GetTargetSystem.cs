﻿using Gameplay.Army.Components;
using Gameplay.Characters.Components;
using Gameplay.Spawning.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Characters.Systems
{
    public class GetTargetSystem : IEcsRunSystem
    {
        private EcsFilter<ObjectComponent, TargetTag> _objectComponentTargetTag;

        private EcsFilter<ObjectComponent, PlayerTag> _objectComponentPlayerTagFilter;
        private EcsFilter<ObjectComponent, EnemyTag> _objectComponentEnemyTagFilter;

        private EcsFilter<ObjectComponent, MovingComponent> _objectComponentMovingComponentFilter;

        public void Run()
        {
            foreach (var index in _objectComponentTargetTag)
            {
                ref var entity = ref _objectComponentTargetTag.GetEntity(index);
                ref var objectComponent = ref _objectComponentTargetTag.Get1(index);

                ref var targetingTag = ref entity.Get<TargetTag>();

                entity.Del<TargetTag>();

                var targetPosition = objectComponent.GameObject.transform;
                targetPosition = entity.Has<EnemyTag>()
                    ? GetNearestPlayerPosition(targetPosition.transform.position)
                    : GetNearestEnemyPosition(targetPosition.transform.position);

                ref var movingComponent = ref entity.Get<MovingComponent>();
                movingComponent.Target = targetPosition;
                movingComponent.AttackRange = 2.0f;
                //TODO ADD Characteristic logic usage
            }
        }


        private Transform GetNearestEnemyPosition(Vector3 currentPosition)
        {
            var minimalDistance = 100000.0f;
            Transform characterPosition = null;
            foreach (var index in _objectComponentEnemyTagFilter)
            {
                ref var objectComponent = ref _objectComponentEnemyTagFilter.Get1(index);

                var distanceToCharacter =
                    (Vector3.Distance(currentPosition, objectComponent.GameObject.transform.position));
                if (distanceToCharacter < minimalDistance)
                {
                    minimalDistance = distanceToCharacter;
                    characterPosition = objectComponent.GameObject.transform;
                }
            }

            return characterPosition;
        }

        private Transform GetNearestPlayerPosition(Vector3 currentPosition)
        {
            var minimalDistance = 100000.0f;
            Transform characterPosition = null;
            foreach (var index in _objectComponentPlayerTagFilter)
            {
                ref var objectComponent = ref _objectComponentPlayerTagFilter.Get1(index);

                var distanceToCharacter =
                    (Vector3.Distance(currentPosition, objectComponent.GameObject.transform.position));
                if (distanceToCharacter < minimalDistance)
                {
                    minimalDistance = distanceToCharacter;
                    characterPosition = objectComponent.GameObject.transform;
                }
            }

            return characterPosition;
        }
    }
}