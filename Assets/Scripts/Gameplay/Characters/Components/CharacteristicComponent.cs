﻿namespace Gameplay.Characters.Components
{
    public struct CharacteristicComponent
    {
        public float Health;
        public float Damage;
        public float AttackRange;
    }
}