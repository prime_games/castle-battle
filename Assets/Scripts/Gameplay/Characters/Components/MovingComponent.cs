﻿using UnityEngine;

namespace Gameplay.Characters.Components
{
    public struct MovingComponent
    {
        public Transform Target;
        public float AttackRange;
    }
}