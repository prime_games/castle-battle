﻿using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Characters.Components
{
    public struct AttackComponent
    {
        public GameObject TargetGameObject;
        public EcsEntity TargetEntity;
    }
}