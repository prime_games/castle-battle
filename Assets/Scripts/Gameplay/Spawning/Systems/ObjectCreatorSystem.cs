﻿using Base.View;
using Gameplay.Army.Components;
using Gameplay.Characters.Components;
using Gameplay.Characters.Configs;
using Gameplay.Spawning.Components;
using Gameplay.Spawning.Configs;
using Leopotam.Ecs;
using UnityEngine;

namespace Gameplay.Spawning.Systems
{
    public class ObjectCreatorSystem : IEcsRunSystem
    {
        private EcsFilter<ObjectComponent> _objectComponentFilter;
        private EcsFilter<ObjectComponent, TargetTag> _objectComponentTargetTag;

        private EcsFilter<ObjectComponent, PlayerTag> _objectComponentPlayerTagFilter;
        private EcsFilter<ObjectComponent, EnemyTag> _objectComponentEnemyTagFilter;

        private EcsFilter<ObjectComponent, MovingComponent> _objectComponentMovingComponentFilter;

        private EcsFilter<ObjectComponent, CharacteristicComponent> _objectComponentCharacteristicComponentFilter;        

        private float _gameTime = 30.0f;
        private float _timeOut = 2.0f;
        private float _nextSpawnTime;

        private CharacterConfig _characterConfig;
        private CharacteristicConfig _characteristicConfig;

        private EcsWorld _world;

        public ObjectCreatorSystem(CharacterConfig characterConfig, CharacteristicConfig characteristicConfig)
        {
            _characterConfig = characterConfig;
            _characteristicConfig = characteristicConfig;
        }

        public void Run()
        {
            if (Time.time > _nextSpawnTime)
            {
                CreateObject();
                _nextSpawnTime = Time.time + _timeOut;
            }
        }

        private void CreateObject()
        {
            var objectEntity = _world.NewEntity();
            ref var objectComponent = ref objectEntity.Get<ObjectComponent>();

            var randomPosition = new Vector3(Random.Range(-4.5f, 4.4f), 1f, Random.Range(5.5f, 14.5f));
            objectComponent.GameObject =
                Object.Instantiate(_characterConfig.Prefab, randomPosition, Quaternion.identity);

            var entityHolder = objectComponent.GameObject.AddComponent<EntityHolder>();
            entityHolder.Entity = objectEntity;
            
           AddCharacteristicComponent(ref objectEntity, _characteristicConfig); 
        }

        private void AddCharacteristicComponent(ref EcsEntity entity, CharacteristicConfig characteristicConfig)
        {
            ref var characteristicComponent = ref entity.Get<CharacteristicComponent>();
            characteristicComponent.Damage = characteristicConfig.Damage;
            characteristicComponent.Health = characteristicConfig.Health;
            characteristicComponent.AttackRange = characteristicConfig.AttackRange;
        }
    }
}