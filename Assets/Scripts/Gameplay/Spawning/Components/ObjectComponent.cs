﻿using UnityEngine;

namespace Gameplay.Spawning.Components
{
    public struct ObjectComponent
    {
        public GameObject GameObject;
        public bool InsideArmy;
    }
}