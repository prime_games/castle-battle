﻿using UnityEngine;

namespace Gameplay.Spawning.Configs
{
    [CreateAssetMenu(fileName = "CharacterConfig", menuName = "Gameplay/Spawning/Configs/CharacterConfig", order = 0)]
    public class CharacterConfig : ScriptableObject
    {
        [SerializeField] private GameObject prefab;

        public GameObject Prefab => prefab;
    }
}