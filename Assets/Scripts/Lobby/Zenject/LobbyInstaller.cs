﻿using Gameplay.Characters.Configs;
using Gameplay.Spawning.Configs;
using Lobby.Settings.System;
using Zenject;

namespace Lobby.Zenject
{
    public class LobbyInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<SettingsSystem>().AsSingle();

            Container.Bind<CharacterConfig>().FromResource("CharacterConfig");
            Container.Bind<CharacteristicConfig>().FromResource("CharacteristicConfig");

            Container.BindInterfacesAndSelfTo<LobbySystemsExecutor>().AsSingle().NonLazy();
        }
    }
}