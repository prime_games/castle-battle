﻿using System;
using Leopotam.Ecs;
using Lobby.Settings.System;
using Zenject;

namespace Lobby.Zenject
{
    public class LobbySystemsExecutor : ITickable, IDisposable
    {
        private readonly EcsSystems _systems;

        public LobbySystemsExecutor(EcsWorld world, SettingsSystem settingsSystem)
        {
            _systems = new EcsSystems(world, "Lobby Systems");

            _systems.Add(settingsSystem);
            _systems.Init();
        }

        public void Dispose()
        {
            _systems.Destroy();
        }

        public void Tick()
        {
            _systems.Run();
        }
    }
}