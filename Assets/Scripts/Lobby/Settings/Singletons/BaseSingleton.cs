﻿using UnityEngine;

namespace PG.Common
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        #region Fields

        public static T instance;

        #endregion

        #region Properties

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    (FindObjectOfType<T>() as BaseSingleton<T>)?.Init();
                }

                return instance;
            }
        }

        #endregion

        #region Unity methods

        protected virtual void Awake()
        {
            Init();
        }

        #endregion

        #region Methods

        protected virtual void Init()
        {
            if (!instance)
            {
                instance = this as T;
            }
        }

        #endregion
    }
}
