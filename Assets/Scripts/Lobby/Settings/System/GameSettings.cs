﻿namespace Lobby.Settings.System
{
    public static class GameSettings
    {
        public static bool SoundIsMuted;
        public static bool VibrationIsActive = true;
    }
}