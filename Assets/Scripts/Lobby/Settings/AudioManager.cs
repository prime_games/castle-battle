﻿using System;
using System.Collections;
using PG.Common;
using UnityEngine;
using UnityEngine.Audio;

namespace Lobby.Settings
{
    [AddComponentMenu("PG.Common/Audio/Audio manager")]
    [DisallowMultipleComponent]
    public sealed class AudioManager : PersistantSingleton<AudioManager>
    {
        #region Fields
    
        [SerializeField] private AudioSource mainMusic;
        [SerializeField] private AudioSource auxMusic;
        [SerializeField] private AudioSource ambientSound;
        [SerializeField] private AudioSource eventSound;
    
        [Space(10)]
        public AudioMixerSnapshot EventSnapshot;
        public AudioMixerSnapshot IdleSnapshot;
    
        [HideInInspector] public AudioMixerSnapshot CurrentSnapshot;
        [HideInInspector] public bool EventRunning;
        [HideInInspector] public bool AuxIn;
    
        #endregion
    
        #region Properties
    
        public AudioSource EventSound => eventSound;
    
        #endregion
    
        #region Common methods
    
        public void PlayEventSound(AudioClip clip)
        {
            if (!clip)
            {
                return;
            }
    
            eventSound.PlayOneShot(clip);
        }
    
        public void SetMusic(bool argument)
        {
            mainMusic.mute = argument;
        }
    
        public void SetSound(bool argument)
        {
            ambientSound.mute = argument;
            eventSound.mute = argument;
            auxMusic.mute = argument;
            OnAudioSettingsChanged?.Invoke();
        }
    
        public void MuteAll()
        {
            SetAllVolumesTo(0);
        }
    
        public void MuteMusic()
        {
            mainMusic.mute = true;
        }
    
        public void UnMuteMusic()
        {
            mainMusic.mute = false;
        }
    
        public void MuteSound()
        {
            ambientSound.mute = true;
            eventSound.mute = true;
            auxMusic.mute = true;
            OnAudioSettingsChanged?.Invoke();
        }
    
        public void UnMuteSound()
        {
            ambientSound.mute = false;
            eventSound.mute = false;
            auxMusic.mute = false;
            OnAudioSettingsChanged?.Invoke();
        }
        
        
        public event Action OnAudioSettingsChanged;
    
        public void UmuteAll()
        {
            SetAllVolumesTo(1);
        }
    
        private void SetAllVolumesTo(float value = 0)
        {
            var sounds = new AudioSource[] { mainMusic, auxMusic, ambientSound, eventSound };
            global::System.Array.ForEach(sounds, s => s.volume = value);
        }
    
        public void ChangeThemeMusic(AudioClip clip)
        {
            ChangeSoundclip(mainMusic, clip);
        }
    
        public void ChangeAuxMusic(AudioClip clip)
        {
            ChangeSoundclip(auxMusic, clip);
        }
    
        private void ChangeSoundclip(AudioSource source, AudioClip clip)
        {
            if (!source || !clip)
            {
                return;
            }
    
            source.clip = clip;
            source.Play();
        }
    
        public void PlayEventSound()
        {
            // StartCoroutine(PlayEvent());
        }
    
        public IEnumerator PlayEvent()
        {
            EventRunning = true;
    
            EventSnapshot.TransitionTo(0.25f);
            CurrentSnapshot = EventSnapshot;
    
            yield return new WaitForSeconds(0.3f);
    
            eventSound.Play();
    
            while (eventSound.isPlaying)
            {
                yield return null;
            }
    
            EventRunning = false;
            //idleSnapshot.TransitionTo(0.5f);
    
            yield break;
        }
    
        #endregion
    }
}
