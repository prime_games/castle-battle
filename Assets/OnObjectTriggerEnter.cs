﻿using System;
using Base.View;
using Gameplay.Army.Components;
using Leopotam.Ecs;
using UnityEngine;

public class OnObjectTriggerEnter : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Character"))
        {
            var dragObject = other.gameObject.GetComponent<DragObject>();
            if (dragObject != null)
            {
                dragObject.OnMouseUpEvent.AddListener(() => RemoveDragObjectScript(other.gameObject));
                dragObject.OnMouseUpEvent.AddListener(() => ReColorObject(other.gameObject));
                dragObject.OnMouseUpEvent.AddListener(() => AddTag(other.gameObject, gameObject.CompareTag("PlayerTag")));
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Character"))
        {
            var dragObject = other.gameObject.GetComponent<DragObject>();
            if (dragObject != null)
            {
                dragObject.OnMouseUpEvent.RemoveAllListeners();
            }
        }
    }

    private void ReColorObject(GameObject triggeredObject)
    {
        var renderer = triggeredObject.GetComponent<Renderer>();
        if (renderer != null)
        {
            renderer.material.color = gameObject.transform.parent.GetComponent<Renderer>().material.color;
        }
    }

    private void RemoveDragObjectScript(GameObject triggeredGameObject)
    {
        var dragObject = triggeredGameObject.GetComponent<DragObject>();
        if (dragObject != null)
        {
            Destroy(dragObject);
        }
    }

    private void AddTag(GameObject gameObject, bool isPlayer)
    {
        var entityHolder = gameObject.GetComponent<EntityHolder>();
        if (isPlayer)
        {
            ref var playerTag = ref entityHolder.Entity.Get<PlayerTag>();
            Debug.Log("Player tag added");
        }
        else
        {
            ref var enemyTag = ref entityHolder.Entity.Get<EnemyTag>();
            Debug.Log("Enemy tag added");
        }
    }
}